import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0

Item {
  id: root

  signal placeStone( point pos )

  function getTurnText() {

    var text
    switch(go.turn) {
      case go.noStone:    text = "None";  break
      case go.whiteStone: text = "White"; break
      case go.blackStone: text = "Black"; break
    }
    return text
  }

  function getModeText() {

    var text
    switch(go.gameMode) {
      case go.gameModeVsPlayer: text = "vs. Player"; break
      case go.gameModeVsAi:     text = "vs. AI"; break
      case go.gameModeAi:       text = "AI vs. AI"; break
    }
    return text
  }

  RowLayout {
    anchors.fill: parent
    anchors.margins: 50

    BoardView {
      Layout.fillHeight: true
      Layout.preferredWidth: height

      Component.onCompleted: placeStone.connect(root.placeStone)
    }

    ColumnLayout {
      Layout.fillWidth: true
      Layout.fillHeight: true
      Text{
        Layout.alignment: Qt.AlignLeft
        text: getModeText()
        font.family: "Curlz MT";
        font.pointSize: 16;
        font.bold: true
      }

      RowLayout {
        Layout.fillHeight: true
        Layout.fillWidth: true

        Item {
          width: 20
        }

        GridLayout {
          Layout.fillWidth: true
          columns: 2

          Text{
            text: "Handicap Black: ";
            font.family: "Curlz MT"
          }
          Text{
            text: "0";
            font.family: "Curlz MT"
          }

          Text{
            text: "Handicap White: ";
            font.family: "Curlz MT"
          }
          Text{
            text: "6.5 komi";
            font.family: "Curlz MT"
          }

          Text {
            text: "Turn: ";
            font.family: "Curlz MT"
          }
          Text {

            Component.onCompleted: text = getTurnText()
            font.family: "Curlz MT"
          }

        }
      }
      Item {
        Layout.fillHeight: true
      }
    }
  }

}

