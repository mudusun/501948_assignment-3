#ifndef GO_H
#define GO_H




#include <map>
#include <memory>
#include <utility>
#include <chrono>
#include <list>
#include <vector>
#include <set>





namespace mylib {

namespace go {



  class Board;
  struct GameState;

  class Player;
  class AiPlayer;
  class HumanPlayer;

  class Engine;









  using time_type = std::chrono::duration<int,std::milli>;

  constexpr time_type operator"" _ms (unsigned long long int milli) { return time_type(milli); }
  constexpr time_type operator"" _s  (unsigned long long int sec)   { return time_type(sec*1000); }









  enum class Stonecolor {
    White       = 0,
    Black       = 1
  };

  enum class PlayerType {
    Human       = 0,
    Ai          = 1
  };

  enum class GameMode {
    VsPlayer    = 0,
    VsAi        = 1,
    Ai          = 2
  };

  using Point   = std::pair<int,int>;
  using Size    = std::pair<int,int>;

  class Stone;













  // INVARIANTS:
  //   Board fullfills; what does one need to know in order to consider a given position.
  //   * all stones and their position on the board
  //   * who places the next stone
  //   * was last move a pass move
  //   * meta: blocks and freedoms

  namespace priv {

  class Block;

    class Board_base {
    public:
      using BoardData = std::map<Point,Stone>;

      struct Position {
        BoardData     board;
        Stonecolor    turn;
        bool          was_previous_pass;

        Position () = default;
        explicit Position ( BoardData&& data, Stonecolor turn, bool was_previous_pass);

      }; // END class Position


      // Constructors
      Board_base() = default;
      explicit Board_base( Size size );
      explicit Board_base( BoardData&& data, Stonecolor turn, bool was_previous_pass );



      // Board data
      Size             size() const;
      bool             wasPreviousPass() const;
      Stonecolor            turn() const;

      // Board
      void             resetBoard( Size size );

      // Board
      Size             _size;
      Position         _current;


    //list of blocks
      std::list<std::shared_ptr<Block>> _block;
      std::list<std::shared_ptr<Block>> getBlock() const;
      void addBlock(std::shared_ptr<Block>block);
      void joinBlocks(std::shared_ptr<Block>newBlock, std::shared_ptr<Block>existingBlock);
      std::shared_ptr<Block>findBlock(Point  PointBlock) const;





    }; // END base class Board_base



  } // END "private" namespace priv






  class Board : private priv::Board_base, public std::enable_shared_from_this<Board> {
  public:
    // Enable types
    using Board_base::BoardData;

    using Board_base::Position;

    // Enable constructors
    using Board_base::Board_base;

    // Make visible from Board_base
    using Board_base::resetBoard;
    using Board_base::size;
    using Board_base::wasPreviousPass;
    using Board_base::turn;
      using Board_base::findBlock;


    // Validate
    bool                  isNextPositionValid( Point intersection ) const;

    // Actions
    void                  placeStone( Point intersection );
    void                  passTurn();

    // Stones and positions
    bool                  hasStone( Point intersection ) const;
    Stone                 stone( Point intersection ) const;
    bool                  isSuicide(Point);


  }; // END class Board




















  class Engine : public std::enable_shared_from_this<Engine> {
  public:
    Engine();

    void                            newGame( Size size );
    void                            newGameVsAi( Size size );
    void                            newGameAiVsAi( Size size );
    void                            newGameFromState( Board::BoardData&& board, Stonecolor turn,
                                                      bool was_previous_pass );

    const Board&                    board() const;

    Stonecolor                           turn() const;

    const GameMode&                 gameMode() const;
    const std::shared_ptr<Player>   currentPlayer() const;

    void                            placeStone( Point intersection );
    bool                            validateStone( Point intersection ) const;
    void                            passTurn();

    void                            nextTurn( time_type think_time = 100_ms );
    bool                            isGameActive() const;




  private:
    std::shared_ptr<Board>          _board;

    GameMode                        _game_mode;
    bool                            _active_game;

    std::shared_ptr<Player>         _white_player;
    std::shared_ptr<Player>         _black_player;

  }; // END class Engine


































} // END namespace go

} // END namespace mylib

namespace mylib{
namespace go {


    class Board;
    namespace priv {


        class Boundary
        {
        public:
        void newpoint(mylib::go::Point _point);

        bool operator < ( const Boundary& other ) const {
            return true;
        }

        private:
        std::vector<Point>_points;
        };

        class Block
        {
        public:
            Block(std::shared_ptr<Board>_board);

            void newstone(Point _point);
            std::set<Boundary>_boundaries;
            std::shared_ptr<Board>_board;
            std::set<Point> getPoints() const;

        private:
            std::set<Point>_stones;

        };





        class stonebase
        {
        public:
            stonebase();
            stonebase(Point _point, Stonecolor _color, std::shared_ptr<Board>board);

            Stonecolor _color;
            Stonecolor checkcolor() const;

            Point _point;
            std::shared_ptr<Board>_board;

            bool hasNorth()const;
            bool hasWest()const;
            bool hasSouth()const;
            bool hasEast()const;

            Point North()const;
            Point West()const;
            Point South()const;
            Point East()const;
        };

    }

    class Stone : private priv::stonebase{
    public:
        using stonebase::North;
        using stonebase::West;
        using stonebase::South;
        using stonebase::East;

        using stonebase::hasNorth;
        using stonebase::hasWest;
        using stonebase::hasSouth;
        using stonebase::hasEast;
        using stonebase::checkcolor;
        using stonebase::stonebase;

};





    }
}

#endif //GO_H
