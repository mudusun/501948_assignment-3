#include "go.h"
#include "randomai.h"
//#include "stonebase.h"

#include <memory>

namespace mylib {
namespace go {



  namespace priv {

    Board_base::Board_base( Size size ) {

      resetBoard(size);
    }

    Board_base::Board_base(Board::BoardData&& data, Stonecolor turn, bool was_previous_pass)
      : _current{std::forward<Board::BoardData>(data),turn,was_previous_pass}
    {
      // ... init ...
    }

    Board_base::Position::Position(Board::BoardData&& data, Stonecolor trn, bool prev_pass)
      : board{data}, turn{trn}, was_previous_pass{prev_pass} {}


    void
    Board_base::resetBoard(Size size) {

      _current.board.clear();
      _size = size;
      _current.turn = Stonecolor::Black;
    }


    //add block

    void Board_base::addBlock(std::shared_ptr<Block> block)
    {
        _block.push_back(block);
    }


    // join block


    void Board_base::joinBlocks(std::shared_ptr<Block> newBlock, std::shared_ptr<Block> existingBlock)
    {
        if(newBlock==existingBlock)
            return;
        addBlock(existingBlock);

        _block.remove(existingBlock);
        // for each stone in newBlock, add stone to existingBlock


    }

    // find positions on the boundaries of existingBlock which the stones of newBlock intrude and remove those from the boundary
    std::shared_ptr<Block> Board_base::findBlock(Point PointBlock) const
    {
        for (std::shared_ptr<Block>block:_block){
            for (auto point:block->getPoints() ){
                if (point==PointBlock){
                        return block;
                }
            }
        }
        return nullptr;
    }


    // add all positions of the boundaries of newBlock, except those which are intruded by existingBlock
    //---




    Size
    Board_base::size() const {

      return _size;
    }

    bool
    Board_base::wasPreviousPass() const {

      return _current.was_previous_pass;
    }

    Stonecolor
    Board_base::turn() const {

      return _current.turn == Stonecolor::Black ? Stonecolor::White : Stonecolor::Black;
    }

  }

bool
Board::isSuicide(Point intersection){
 Stone newStone = Stone {intersection, _current.turn, shared_from_this()};

   int count = 0;
   if(newStone.hasNorth())
   {
       if(newStone.checkcolor()!=this->stone(newStone.North()).checkcolor())
          count++;
   }
   if(newStone.hasEast())
   {
       if(newStone.checkcolor()!=this->stone(newStone.East()).checkcolor())
          count++;
   }
   if(newStone.hasSouth())
   {
       if(newStone.checkcolor()!=this->stone(newStone.South()).checkcolor())
          count++;
   }
   if(newStone.hasWest())
   {
       if(newStone.checkcolor()!=this->stone(newStone.West()).checkcolor())
          count++;
   }
   if(count==4)
       return true;
   else
       return false;
}

  void
  Board::placeStone(Point intersection) {

      if(isNextPositionValid(intersection) && !this->isSuicide(intersection))
      {

    _current.board[intersection] = Stone {intersection, _current.turn, std::shared_ptr<Board>(this)};


  //make a new block
      auto blockObj = priv::Block{std::shared_ptr<Board>(this)};
      auto block = std::make_shared<priv::Block>(blockObj);
      blockObj.newstone(intersection);
      addBlock(block);
      Stone a = stone(intersection);
      //check if there is a block for the north and add it

      if(a.hasNorth()){
        if(this->stone(a.North()).checkcolor()==a.checkcolor())
           joinBlocks(block,findBlock(a.North()));

        if(a.hasWest()){
          if(this->stone(a.West()).checkcolor()==a.checkcolor())
             joinBlocks(block,findBlock(a.West()));

          if(a.hasSouth()){
            if(this->stone(a.South()).checkcolor()==a.checkcolor())
               joinBlocks(block,findBlock(a.South()));

          if(a.hasEast()){
            if(this->stone(a.East()).checkcolor()==a.checkcolor())
               joinBlocks(block,findBlock(a.East()));
          }

         }
        }
      }




     _current.turn = turn();
      }
  }



  void
  Board::passTurn() {

    _current.turn = turn();
  }

  bool
  Board::hasStone(Point intersection) const {

    return _current.board.count(intersection);
  }

  Stone
  Board::stone(Point intersection) const {

    return _current.board.at(intersection);
  }


  bool
  Board::isNextPositionValid(Point intersection) const {

      if(hasStone(intersection))
         return false;
       return true;

  }

  Engine::Engine()
    : _board{std::make_shared<Board>()}, _game_mode{}, _active_game{false},
      _white_player{nullptr}, _black_player{nullptr} {}

  void
  Engine::newGame(Size size) {

    _board->resetBoard(size);

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),Stonecolor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),Stonecolor::White);
  }

  void
  Engine::newGameVsAi(Size size) {

    _board->resetBoard(size);

    _game_mode = GameMode::VsAi;
    _active_game = true;

    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),Stonecolor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),   Stonecolor::White);
  }

  void
  Engine::newGameAiVsAi(Size size) {

    _board->resetBoard(size);

    _game_mode = GameMode::Ai;
    _active_game = true;

    _white_player = std::make_shared<RandomAi>(shared_from_this(),Stonecolor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),Stonecolor::White);
  }

  void
  Engine::newGameFromState(Board::BoardData&& board, Stonecolor turn, bool was_previous_pass) {

    _board = std::make_shared<Board>(Board {std::forward<Board::BoardData>(board),turn,was_previous_pass});

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),Stonecolor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),Stonecolor::White);
  }

  const Board&
  Engine::board() const {

    return *_board;
  }

  const GameMode&
  Engine::gameMode() const {

    return _game_mode;
  }

  Stonecolor
  Engine::turn() const {

    return _board->turn();
  }

  const std::shared_ptr<Player>
  Engine::currentPlayer() const {

    if(      turn() == Stonecolor::Black ) return _black_player;
    else if( turn() == Stonecolor::White ) return _white_player;
    else                              return nullptr;
  }

  void
  Engine::placeStone(Point intersection) {

    _board->placeStone(intersection);
  }

  void
  Engine::passTurn() {

    if(board().wasPreviousPass()) {
      _active_game = false;
      return;
    }

    _board->passTurn();
  }

  void
  Engine::nextTurn(std::chrono::duration<int,std::milli> think_time) {

    if( currentPlayer()->type() != PlayerType::Ai) return;

    auto p = std::static_pointer_cast<AiPlayer>(currentPlayer());

    p->think( think_time );
    if( p->nextMove() == AiPlayer::Move::PlaceStone )
      placeStone( p->nextStone() );
    else
      passTurn();
  }

  bool
  Engine::isGameActive() const {

    return _active_game;
  }

  bool
  Engine::validateStone(Point pos) const {

    return _board->isNextPositionValid(pos);
  }


  namespace priv {





      stonebase::stonebase()
      {

      }

      stonebase::stonebase(Point _point, Stonecolor _color, std::shared_ptr<Board> board)
      {this->_point=_point;
       this->_color=_color;
       this->_board=board;

      }

      Stonecolor stonebase::checkcolor() const
      {return _color;

      }


      bool stonebase::hasNorth() const
      {
          Point a = Point(_point.first,_point.second-1);
          return _board->hasStone(a);
      }

      bool stonebase::hasWest() const
      {
          Point a = Point(_point.first-1,_point.second);
          return _board->hasStone(a);
      }

      bool stonebase::hasSouth() const
      {
          Point a =  Point(_point.first,_point.second+1);
          return _board->hasStone(a);
      }

      bool stonebase::hasEast() const
      {
          Point a =  Point(_point.first+1,_point.second);
          return _board->hasStone(a);
      }

      Point stonebase::North() const
      {
          return Point(_point.first,_point.second-1);
      }

      Point stonebase::West() const
      {
          return Point(_point.first-1,_point.second);
      }

      Point stonebase::South() const
      {
          return Point(_point.first,_point.second+1);
      }

      Point stonebase::East() const
      {
          return Point(_point.first+1,_point.second);
      }


      Block::Block(std::shared_ptr<Board> _board)
      {
          this->_board=_board;
      }

      void Block::newstone(Point _point)
      {
          _stones.insert(_point);

         // Boundary a;

          //std::set<std::shared_ptr<Boundary>> h,


           std::shared_ptr<Boundary>newboundary {std::make_shared<Boundary>() };
           Boundary boundary = Boundary {};
           //insert north
           if(_point.first,_point.second-1>0)
               boundary.newpoint(Point(_point.first, _point.second-1));

           //insert west
           if(_point.first-1>0,_point.second)
               boundary.newpoint(Point(_point.first-1, _point.second));

           //insert south

           if(_point.second<=_board->size().second)

               boundary.newpoint(Point(_point.first, _point.second+1));

           //insert east
           if(_point.first<=_board->size().first)
               boundary.newpoint(Point(_point.first+1, _point.second));


         _boundaries.insert(*newboundary);


      }

      std::set<Point> Block::getPoints() const
      {
         return _stones;
      }
      void Boundary::newpoint(Point _point)
      {
          _points.push_back(_point);
      }


  }












} // END namespace go
} // END namespace mylib
