// My Go Engine
#include "../../mylibrary/go.h"

// gtest
#include <gtest/gtest.h>



namespace go = mylib::go;

//test for stone duplicate
TEST(PlaceStone,DuplicateStone) {

    go::Board::BoardData board_data;
    board_data[go::Point(3,3)] = go::Stone{go::Point(3,3), go::Stonecolor::Black, nullptr};

    // Create a engine from state
    go::Board board {std::move(board_data),go::Stonecolor::White,false};

    board.placeStone(go::Point(3,3));

    EXPECT_TRUE(board.stone(go::Point(3,3)).checkcolor()==go::Stonecolor::Black);


}


//if it is allowed to place stone there
TEST(checkStone,isNextPositionValid){
    go::Board::BoardData board_data;
    board_data[go::Point(3,3)] = go::Stone{go::Point(3,3), go::Stonecolor::Black, nullptr};

    // Create a engine from state
    go::Board board {std::move(board_data),go::Stonecolor::White,false};

    EXPECT_FALSE(board.isNextPositionValid(go::Point(3,3)));
}


/*
//if I put the stone successfully
TEST(checkStone,stone){
    go::Board::BoardData board_data;
    go::Stone stone= go::Stone{go::Point(3,3), go::Stonecolor::Black, nullptr};

    board_data[go::Point(3,3)] = stone;
    // Create a engine from state
    go::Board board {std::move(board_data),go::Stonecolor::White,false};

    EXPECT_EQ(stone,board.stone(go::Point(3,3)));
}
*/

//check the suicide
TEST(checkStone,isSuicide){
    go::Board::BoardData board_data;
    board_data[go::Point(2,3)] = go::Stone{go::Point(2,3), go::Stonecolor::Black, nullptr};
    board_data[go::Point(3,2)] = go::Stone{go::Point(3,2), go::Stonecolor::Black, nullptr};
    board_data[go::Point(4,3)] = go::Stone{go::Point(4,3), go::Stonecolor::Black, nullptr};
    board_data[go::Point(3,4)] = go::Stone{go::Point(3,4), go::Stonecolor::Black, nullptr};
    // Create a engine from state
    go::Board board {std::move(board_data),go::Stonecolor::White,false};
    EXPECT_TRUE(board.isSuicide(go::Point(2,2)));
}


//check if addblock works
TEST(buildBlock,addBlock){

    go::Board board {go::Size(5,5)};
    board.placeStone(go::Point(3,3));

    auto block = board.findBlock(go::Point(3,3));

    EXPECT_EQ(1, block->getPoints().size());
}

